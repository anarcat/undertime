# bash completion for undertime                             -*- shell-script -*-

_timezones () {
    python3 -c "import pytz; print(' '.join(pytz.all_timezones))"
}

_undertime () {
    local cur prev words
    cur=$2
    prev=$3
    COMPREPLY=() 

    words=( ${COMP_LINE[@]} )
    # If the last option argument is -t or --timezones and the point is at
    # a position after -t or --timezones then complete timezone names
    # otherwise complete regular options
    for ((i=${#words[@]} - 1; i > 0; i--)); do
        if [[ ${words[i]} == -* ]]; then
            if [[ ${words[i]} == @(-t|--timezones) ]]; then
                rest=${COMP_LINE#*${words[i]}}
                if (( $COMP_POINT > ${#rest}  )); then
                    COMPREPLY=( $(compgen -W "$(_timezones)" -- $cur) )
                    return 0
                fi
            fi
            # give up if we have an option other than -t or --timezones
            break
        fi
    done
        
    case "$prev" in
        --config)
            _filedir @(yml|yaml)
            return 0
            ;;
        --format|-f)
            COMPREPLY=( $(compgen -W 'fancy_grid github grid html jira \
                                latex latex_booktabs latex_raw mediawiki \
                                moinmoin orgtbl pipe plain presto pretty \
                                psql rst simple textile tsv youtrack \
                                fancy_grid_nogap' -- $cur ) )
            return 0
            ;;
        --start|-s)
            ;&
        --end|-e)
            ;&
        --overlap-min)
            return 0
            ;;
    esac
    COMPREPLY=( $( compgen -W  '\
    -t --timezones \
    -s --start \
    -e --end \
    --no-colors --colors \
    --no-default-zone --default-zone \
    --no-unique --unique \
    --no-overlap --overlap \
    --overlap-min \
    --truncate --no-truncate \
    --table --no-table \
    -f --format \
    --config \
    -v --verbose \
    --debug \
    -l --list-zones \
    -V --version \
    -h --help\
    ' -- $cur) )

    return 0
} &&
    complete -F _undertime undertime

# ex filetype=sh
