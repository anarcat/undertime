#!/bin/sh

# this is a shell script because using a Python command doesn't
# actually work: the TZ variable affects the datetime module at module
# discovery time


export TZ=:Asia/Singapore

# this used to crash with:
# pytz.exceptions.UnknownTimeZoneError: '+08'
#
# see also https://github.com/scrapinghub/dateparser/issues/1192
undertime --config /dev/null --default-zone -- tomorrow
